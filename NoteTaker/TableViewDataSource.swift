//
//  TableViewDataSource.swift
//  NoteTaker
//
//  Created by mark on 2/25/19.
//  Copyright © 2019 Swift Dev Journal. All rights reserved.
//

import Cocoa

extension NotesViewController: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        // Return the number of notes
        if let appDelegate = NSApplication.shared.delegate as? AppDelegate {
            return appDelegate.notes.count
        }
        return 0
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        if let result = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "TitleColumn"), owner: self) as? NSTableCellView {
            
            // Set the table view text field to the note's title
            if let appDelegate = NSApplication.shared.delegate as? AppDelegate {
                result.textField?.stringValue = appDelegate.notes[row].title
            }
            return result
        }
        
        
        return nil
    }
}
