//
//  NoteSplitViewController.swift
//  NoteTaker
//
//  Created by mark on 2/26/19.
//  Copyright © 2019 Swift Dev Journal. All rights reserved.
//

import Cocoa

class NoteSplitViewController: NSSplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    func getNotesViewController() -> NotesViewController? {
        if let viewController = children.first as? NotesViewController {
            return viewController
        }
        return nil
    }
    
    func getTextViewController() -> TextViewController? {
        if let viewController = children[1] as? TextViewController {
            return viewController
        }
        return nil
    }
}
