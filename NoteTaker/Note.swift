//
//  Note.swift
//  NoteTaker
//
//  Created by mark on 2/25/19.
//  Copyright © 2019 Swift Dev Journal. All rights reserved.
//

import Foundation

struct Note {
    var title: String
    var contents: NSMutableAttributedString
}
